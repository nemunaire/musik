#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from gearman import GearmanWorker
from xml.dom.minidom import getDOMImplementation, parseString

from musik import *

def gen_response(url):
    if not is_valid_URL(url):
        return gen_error(-1, "Aucun extracteur n'a été trouvé pour cette URL")

    else:
        impl = getDOMImplementation()

        newdoc = impl.createDocument(None, "gearman_musik", None)
        root = newdoc.documentElement

        dl = get_downloader(url)
        info = get_informations(url)

        res = newdoc.createElement("response")
        rest = newdoc.createTextNode(download_video(dl, info["formats"][0]).encode('utf8', 'replace'))
        res.appendChild(rest)
        root.appendChild(res)

        return newdoc.toxml()

gm_worker = GearmanWorker(['localhost:4730'])

def fetch_movie(worker, job):
    inputdom = parseString(job.data)

    urls = inputdom.documentElement.getElementsByTagName("url")

    if len(urls) > 0:
        return gen_response(urls[0].childNodes[0].data)
    else:
        return gen_error(0, "Pas d'URL à télécharger")

gm_worker.register_task('fetchMovie', fetch_movie)

gm_worker.work()
