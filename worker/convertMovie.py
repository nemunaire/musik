#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from gearman import GearmanWorker
from xml.dom.minidom import getDOMImplementation, parseString

from musik import *

def gen_response(filepath, wformat="best"):
    impl = getDOMImplementation()

    newdoc = impl.createDocument(None, "gearman_musik", None)
    root = newdoc.documentElement

    res = newdoc.createElement("response")
    rest = newdoc.createTextNode(convert_file(filepath, wformat))
    res.appendChild(rest)
    root.appendChild(res)

    return newdoc.toxml()

gm_worker = GearmanWorker(['localhost:4730'])

def convert_movie(worker, job):
    inputdom = parseString(job.data)

    filenames = inputdom.documentElement.getElementsByTagName("filename")
    wformat = inputdom.documentElement.getElementsByTagName("format")

    if len(wformat) <= 0:
        wformat = "mp3"

    if len(filenames) > 0:
        return gen_response(filenames[0].childNodes[0].data)
    else:
        return gen_error(0, "Pas de fichier à convertir")

gm_worker.register_task('convertMovie', convert_movie)

gm_worker.work()
