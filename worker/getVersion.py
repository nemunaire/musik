#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from gearman import GearmanWorker
from xml.dom.minidom import getDOMImplementation

import youtube_dl

def gen_response():
    impl = getDOMImplementation()

    newdoc = impl.createDocument(None, "gearman_musik", None)
    root = newdoc.documentElement
    
    version = newdoc.createElement("version")
    versiont = newdoc.createTextNode(youtube_dl.version.__version__)
    version.appendChild(versiont)
    root.appendChild(version)

    return newdoc.toxml()


gm_worker = GearmanWorker(['localhost:4730'])

def get_version(gearman_worker, gearman_job):
    return gen_response()

gm_worker.register_task('getVersion', get_version)

gm_worker.work()
