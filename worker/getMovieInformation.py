#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from gearman import GearmanWorker
from xml.dom.minidom import getDOMImplementation, parseString

from musik import *

def gen_response(url):
    if not is_valid_URL(url):
        return gen_error(-1, "Aucun extracteur n'a été trouvé pour cette URL")

    else:
        impl = getDOMImplementation()

        newdoc = impl.createDocument(None, "gearman_musik", None)
        root = newdoc.documentElement

        try:
            dict_to_xml(get_informations(url), root, newdoc)
            root.setAttribute("status", "1")
        except youtube_dl.utils.ExtractorError, e:
            return gen_error(-2, str(e))

        return newdoc.toxml()

gm_worker = GearmanWorker(['localhost:4730'])

def get_information(worker, job):
    inputdom = parseString(job.data)

    urls = inputdom.documentElement.getElementsByTagName("url")

    if len(urls) > 0:
        return gen_response(urls[0].childNodes[0].data)
    else:
        return gen_error(0, "Pas d'URL à rechercher")

gm_worker.register_task('getMovieInformation', get_information)

gm_worker.work()
