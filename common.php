<?php

$user = "generic";
foreach ($_GET as $k => $t)
{
  if (empty($t))
  {
    $user = $k;
    break;
  }
}
if (!preg_match("#^[a-zA-Z0-9_]+$#", $user))
  die ("Le nom d'utilisateur contient des caractères interdits.");


function get_info($url)
{
  $ec = file(MAIN_DIR."/database",FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
  $nbLign = count($ec);

  for ($i = 0; $i < $nbLign; $i++)
  {
    if ($ec[$i] == $url)
      break;
    else
    {
      while ($i < $nbLign && trim($ec[$i]) != "--")
        $i++;
    }
  }

  if ($i+2 < $nbLign)
  {
    if ($ec[$i+3] == "-- ")
    {
      @$vfilename = $ec[$i+2];
      @$pic = "default.jpg";
    }
    else
    {
      @$pic = $ec[$i+2];
      @$vfilename = $ec[$i+3];
    }

    $mp3 = preg_replace("#^(.+)\.([a-zA-Z0-9]{1,4})$#",
                             '\1.mp3',
                             $vfilename);
    $ogg = preg_replace("#^(.+)\.([a-zA-Z0-9]{1,4})$#",
                                '\1.ogg',
                                $vfilename);

    return @array("url" => $ec[$i],
                  "title" => $ec[$i+1],
                  "pics" => $pic,
                  "filename" => $vfilename,
                  "mp3" => trim($mp3),
                  "ogg" => trim($ogg));
  }
  else
    return NULL;
}

function add_url($user, $url)
{
  if (
    preg_match("#^http://(www.)?youtube.com/watch\?v=([a-zA-Z0-9_-]+)#",
               $url, $matched)
    || preg_match("#^http://(www.)?youtube.com/playlist\?list=([a-zA-Z0-9_-]+)#",
               $url, $matched)
    || preg_match("#^http://youtu.be/([a-zA-Z0-9_-]+)#",
                  $url, $matched)
    || preg_match("#^http://(www.)?dailymotion.com/video/([a-zA-Z0-9_-]+)#",
                  $url, $matched)
    || preg_match("#^http://(www.)?vimeo.com/([0-9]+)#",
                  $url, $matched)
    )
  {
    //Check if the URL isn't already in the file
    if (is_file(MAIN_DIR."/users/".$user.".dlist"))
    {
      $content = file(MAIN_DIR."/users/".$user.".dlist",
                      FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
      if (in_array($matched[0], $content))
      {
        print "<h3>Le fichier est déjà dans la liste d'attente.</h3>";
        return;
      }
    }

    //Is the file already downloaded by another user?
    $video = get_info($matched[0]);
    if (!empty($video))
    {
      $fp = fopen(MAIN_DIR."/users/".$user.".dlist.done", "a+");
      fputs($fp, $matched[0]."\n");
      fclose($fp);
      print '<h3 style="color: #0000FF;">Vidéo déjà convertie...<meta http-equiv="refresh" content="2"></h3>';
      return;
    }

    if ($fp = fopen(MAIN_DIR."/users/".$user.".dlist", "a+"))
    {
      fputs($fp, $matched[0]."\n");
      fclose($fp);
      print "<h3 style=\"color: #00FF00;\">L'adresse a bien été ajoutée avec succès.</h3>";
    }
    else
      die("Une erreur s'est produite lors de l'ajout de la vidéo au fichier ; il s'agit sans doute d'un problème de droits. Contactez l'administrateur du site pour qu'il corrige le problème.");
  }
  else
    print "<h3>L'adresse fournie n'est pas valide !</h3>";
}

function mark_as_dled($user, $key)
{
  $dir = MAIN_DIR."/content/";

  if (is_file(MAIN_DIR."/users/".$user.".dlist.done"))
  {
    $ec = file(MAIN_DIR."/users/".$user.".dlist.done",
               FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $outstr = "";

    foreach ($ec as $k => $lign)
    {
      if ($key == $k)
      {
        preg_match("#^([^ ]+)( ([0-9]+))?#", $lign, $out);

        if (empty($out[2]))
          $outstr .= $lign." ".time()."\n";
        else
          $outstr .= $lign."\n";
      }
      else
        $outstr .= $lign."\n";
    }

    file_put_contents(MAIN_DIR."/users/".$user.".dlist.done", $outstr);
  }
}

function get_dlmusiks($user)
{
  $list = array();

  $dir = MAIN_DIR."/content/";

  if (is_file(MAIN_DIR."/users/".$user.".dlist.done"))
  {
    $ec = file(MAIN_DIR."/users/".$user.".dlist.done",
               FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    foreach ($ec as $k => $lign)
    {
      preg_match("#^([^ ]+)( ([0-9]+))?#", $lign, $out);

      if (empty($out[2]))
        $out[2] = "0";

      $musik = get_info($out[1]);
      if(isset($musik)
         && (is_file($dir.'/'.$musik["mp3"])
             || is_file($dir.'/'.$musik["ogg"])))
      {
        $musik["dled"] = intval($out[2]);
        $musik["k"] = $k;
        $list[$k] = $musik;
      }
    }
  }

  return $list;
}

function get_user_ready($user)
{
  $list = array();

  foreach (get_dlmusiks($user) as $k => $lign)
  {
    if (!$lign["dled"])
      $list[$k] = $lign;
  }

  return array_reverse($list);
}

function get_user_archives($user)
{
  $list = array();
  $sort = array();

  if ($user != "generic")
    foreach (get_dlmusiks($user) as $k => $lign)
    {
      if ($lign["dled"])
      {
        $sort[] = $lign["dled"];
        $list[$k] = $lign;
      }
    }

  array_multisort($sort, $list);

  return $list;
}

function get_ecdl($user)
{
  $list = array();

  if (is_file(MAIN_DIR."/users/".$user.".dlist.ec"))
  {
    $ec = file(MAIN_DIR."/users/".$user.".dlist.ec",
               FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    if (!empty($ec[0]))
    {
      $musik = get_info($ec[0]);
      if(isset($musik))
      {
        if (!empty($musik["pics"]))
          echo '<div style="text-align: center;"><img src="'.
            $musik["pics"].'" alt="Miniature"></div>';

        if (count ($ec) == 1)
          echo "<label>En cours de téléchargement :</label>";
        else if (count ($ec) == 2)
          echo "<label>En cours de conversion :</label>";

        echo $musik["title"];
        $encours = $ec[0];
      }
    }
  }

  if (is_file(MAIN_DIR."/users/".$user.".dlist"))
  {
    $ec = file(MAIN_DIR."/users/".$user.".dlist",
               FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    foreach ($ec as $k => $lign)
    {
      if (isset($encours) && $lign == $encours)
        continue;

      $list[] = $lign;
    }
  }

  return $list;
}

function format_clip($user, $m)
{
  ?>
  <div class="clip">
    <a href="dl.php?<?php echo $user; ?>&amp;s&amp;f=<?php echo $m["k"]; ?>"
       class="tp"><br>Stream</a>
    <a href="<?php echo $m["url"]; ?>" class="tp">Source</a>
    <img src="<?php echo $m["pics"]; ?>"
         alt="<?php echo $m["title"]; ?>"
         title="<?php echo $m["title"]; ?>">
    <a title="<?php echo $m["title"]; ?>" class="bot"
       href="dl.php?<?php echo $user; ?>&amp;f=<?php echo $m["k"]; ?>">
    <?php echo $m["title"]; ?></a>
  </div>
  <?php
}

?>