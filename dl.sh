#!/bin/bash

pwd=$(dirname $0)

# Don't do anything if this script is already launched
if [ ! -e /tmp/dlEnCours ]; then
    for f in "$pwd"/users/*.dlist; do
        if [ `cat $f | wc -l` -gt 0 ]; then
            touch /tmp/dlEnCours

            lign=`sed '2,$d' "$f" | tr -d '\n'`

            while [ -n "$lign" ] && [ `cat "$f" | wc -l` -gt 0 ]
            do
                if [ -z "$lign" ]; then
	            sed -i '1d' "$f"
	            lign=`sed '2,$d' "$f" | tr -d '\n'`
                fi

	        echo "Action: $lign"
	        if [ "$lign" = "clear" ]; then
                    echo "-- " >> "$f.done"
	        else
                    "$pwd"/youtube-dl --skip-download "$lign"
                    size=`"$pwd"/youtube-dl --skip-download "$lign" | grep "File size"`
                    if echo $size | grep -E " [1234]?[0-9]{1,2}\.[0-9]+M "
                    then
                        echo "$lign" >> "$pwd"/database

                        "$pwd"/youtube-dl --get-title --get-thumbnail --get-filename "$lign" >> "$pwd"/database
                        echo "-- " >> "$pwd"/database

                        echo "$lign" > "$f.ec"
#	                "$pwd"/youtube-dl -c -o "$pwd/content/%(id)s.%(ext)s" "$lign"
                       "$pwd"/youtube-dl --no-progress -c -o "$pwd/content/%(id)s.%(ext)s" "$lign"
                        echo "$lign" >> "$f.ec"
                        echo "${f:$((${#pwd} + 7)):4}"
                        if [ "${f:$((${#pwd} + 7)):4}" == "nemu" ]; then
                            echo "nemu convertion"
	                    "$pwd"/youtube-dl --no-progress -c -k -o "$pwd/content/%(id)s.%(ext)s" --extract-audio --audio-format=vorbis "$lign"
                        else
	                    "$pwd"/youtube-dl --no-progress -c -k -o "$pwd/content/%(id)s.%(ext)s" --extract-audio --audio-format=mp3 "$lign"
                        fi
                        echo "" > "$f.ec"

                        echo "$lign" >> "$f.done"

                        chgrp www-data "$f.done" "$f.ec"
                        chmod g+rw "$f.done" "$f.ec"
                    else
                        echo $size
	            fi
                fi
                #Remove the action from the file
	        sed -i '1d' "$f"
                chgrp www-data "$f"
                chmod g+rw "$f"

                #What's next?
	        lign=`sed '2,$d' "$f" | tr -d '\n'`
            done
        fi
    done
    rm /tmp/dlEnCours 2> /dev/null
fi
