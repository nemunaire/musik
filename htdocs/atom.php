<?php

define("MAIN_DIR", __dir__."/..");

require(MAIN_DIR."/common.php");

//header("Content-type: application/atom+xml;charset=utf-8");

$xml = new DOMDocument('1.0', 'UTF-8');
$xml->formatOutput = true;

$xml_feed = $xml->createElement("feed");
$xml_feed->setAttribute("xmlns", "http://www.w3.org/2005/Atom");

$xml_feed_link = $xml->createElement("link");
$xml_feed_link->setAttribute("rel", "self");
$xml_feed_link->setAttribute("href", "http://".$_SERVER["SERVER_NAME"].dirname($_SERVER["REQUEST_URI"])."atom.php?".$user);

$xml_feed_rights = $xml->createElement("rights", "Pommultimédia Online Converter");
$xml_feed_rights->setAttribute("type", "text");

$xml_feed_author = $xml->createElement("author");
$xml_feed_author->appendChild($xml->createElement("name", "nemunaire"));

$xml_feed->appendChild($xml->createElement("title", "Musiques téléchargées de ".$user));
$xml_feed->appendChild($xml->createElement("updated", date('c')));
$xml_feed->appendChild($xml->createElement("id", "http://musik.p0m.fr/atom.php?".$user));
$xml_feed->appendChild($xml_feed_link);
$xml_feed->appendChild($xml->createElement("generator", "Onyx Atom generator"));
$xml_feed->appendChild($xml_feed_rights);
$xml_feed->appendChild($xml_feed_author);

$files = array();

$ec = get_dlmusiks($user);
$cnt = 0;
for ($i = count ($ec) - 1; $i >= 0 && $cnt < 10; $i--, $cnt++)
{
  $musik = $ec[$i];
  if (isset($musik) && (is_file (MAIN_DIR."/content/".$musik["mp3"]) || is_file (MAIN_DIR."/content/".$musik["ogg"])))
    {
      $xml_entry = $xml->createElement("entry");
      $xml_entry->appendChild($xml->createElement("id", "http://musik.p0m.fr/".$musik["filename"]));
      @$xml_entry->appendChild($xml->createElement("title", strip_tags($musik["title"])));
      if (is_file (MAIN_DIR."/content/".$musik["ogg"]))
        $xml_entry->appendChild($xml->createElement("updated", date('c', filectime(MAIN_DIR."/content/".$musik["ogg"]))));
      else
        $xml_entry->appendChild($xml->createElement("updated", date('c', filectime(MAIN_DIR."/content/".$musik["mp3"]))));

      $xml_entry_summary = $xml->createElement("summary", htmlentities(utf8_decode($musik["title"])));
      $xml_entry_summary->setAttribute("type", "html");

      $xml_entry_enclosure = $xml_entry->appendChild($xml->createElement("link"));
      $xml_entry_enclosure->setAttribute("href", "http://".$_SERVER["SERVER_NAME"].dirname($_SERVER["REQUEST_URI"]).urlencode("dl.php?".$user."&s&f=".$i));
      $xml_entry_enclosure->setAttribute("rel", "enclosure");
      $xml_entry->appendChild($xml_entry_enclosure);

      $xml_entry_enclosure = $xml_entry->appendChild($xml->createElement("link"));
      $xml_entry_enclosure->setAttribute("href", $musik["url"]);
      $xml_entry_enclosure->setAttribute("rel", "via");
      $xml_entry->appendChild($xml_entry_enclosure);

      $xml_entry_link = $xml->createElement("link");
      $xml_entry_link->setAttribute("rel", "alternate");
      $xml_entry_link->setAttribute("href", "http://".$_SERVER["SERVER_NAME"].dirname($_SERVER["REQUEST_URI"])."?".$user);

      $xml_entry->appendChild($xml_entry_summary);
      $xml_entry->appendChild($xml_entry_link);
      $xml_feed->appendChild($xml_entry);
    }
  }


$xml->appendChild($xml_feed);

//Cache::set('flux', array("date" => time(), "flux" => $xml->saveXML()));

print $xml->saveXML();
?>