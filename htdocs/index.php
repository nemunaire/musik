<?php

define("MAIN_DIR", __dir__."/..");

header("Content-type: text/html;charset=utf-8");

require(MAIN_DIR."/common.php");
?>
<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>.: Pommultimédia - Online Converter :.</title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
    <meta http-equiv="refresh" content="42">
  </head>
  <body>
   <script type="text/javascript">
   function toggleDiv(divname) {
     if (document.getElementById(divname).className=="")
       document.getElementById(divname).className="hidden";
     else
       document.getElementById(divname).className="";
   }
   </script>
   <div class="blk">
     <h2>Chansons prêtes à être téléchargées</h2>
     <div class="dled"><?php
$list_ready = get_user_ready($user);
if (count($list_ready))
{
  foreach ($list_ready as $m)
    format_clip($user, $m);
}
else
  echo "<h3>Pas de nouvelle chanson à télécharger</h3>";
?>     </div>
   </div>
   <div class="blk">
     <h2>Ajouter une chanson</h2>
<?php
  if ((!empty($_GET["a"]) && $_GET["a"] == "add"
       && (!empty($_POST["url"]) || !empty($_SERVER["HTTP_REFERER"])))
      || !empty($_GET["url"]))
  {
    if (!empty($_GET["url"]))
      $url = $_GET["url"];
    elseif (!empty($_POST["url"]))
      $url = $_POST["url"];
    else
      $url = $_SERVER["HTTP_REFERER"];
    add_url($user, $url);
  }
?>
     <form method="post" action="?a=add&amp;<?php echo $user; ?>">
       <label for="url">Adresse :</label> <input type="text" name="url" size="50">
       <input type="submit" value="Ajouter">
     </form>
   </div>
   <div class="blk">
     <h2>Chansons en file d'attente</h2>
       <form method="post" action="?a=del">
         <ul><?php
$ecdl = get_ecdl($user);
foreach($ecdl as $lign)
{
  echo '<li>'.$lign.' <input type="submit" name="del'.
  sha1($lign).'" value="Retirer de la liste"></li>';
}
if (empty($ecdl))
  echo "<h3>Aucun élément dans cette liste</h3>";
?></ul>
     </form>
   </div><?php
$list_archives = get_user_archives($user);
if (count($list_archives))
{
  print '<div class="blk"><h2>Chansons déjà téléchargées</h2><div class="dled">';
  $last = 0;
  foreach ($list_archives as $m)
  {
    $ths = intval($m["dled"] / 21600);
    if ($last < $ths)
    {
      if ($last != 0)
        echo "</div>";
      $last = $ths;
      echo "<h4>".strftime("%a. %d %b %G - %H:%M", $m["dled"]).' <a href="javascript:toggleDiv(\'mus'.$m["dled"].'\')">Afficher&hellip;</a></h4><div class="hidden" id="mus'.$m["dled"].'">';
    }
    format_clip($user, $m);
  }
  print '</div></div>';
}
?></body>
</html>
