<?php
define("MAIN_DIR", __dir__."/..");

require(MAIN_DIR."/common.php");

if (isset($_GET["s"]))
  $stream = "inline";
else
  $stream = "attachment";

if (isset($_GET["f"]))
{
  $musiks = get_dlmusiks($user);

  foreach ($musiks as $k => $lign)
  {
    if ($k == $_GET["f"])
    {
      $musik = get_info($lign["url"]);
      if (isset($musik) && (is_file(MAIN_DIR."/content/".$musik["mp3"])
                            || is_file(MAIN_DIR."/content/".$musik["ogg"])))
      {
        if (is_file(MAIN_DIR."/content/".$musik["ogg"]))
        {
          $filename = MAIN_DIR."/content/".$musik["ogg"];
          $ext = ".ogg";
        }
        else
        {
          $filename = MAIN_DIR."/content/".$musik["mp3"];
          $ext = ".mp3";
        }

        //Mark file as dled
        mark_as_dled($user, $k);

        if (is_file(MAIN_DIR."/content/".$musik["ogg"]))
          $fname = MAIN_DIR."/content/".$musik["ogg"];
        else
          $fname = MAIN_DIR."/content/".$musik["mp3"];

        header('Content-Description: File Transfer');
        header('Content-Type: '.mime_content_type($filename));
        header("Content-Disposition: ".$stream."; filename=\"".addslashes($musik["title"]).$ext."\"");
        header('Content-Transfer-Encoding: binary');
        //header('Expires: 0');
        //header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit;
      }
    }
  }
}
die ("Fichier introuvable");
?>
